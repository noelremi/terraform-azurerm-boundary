provider "azurerm" {
  version = "~> 2.8"
  features {} # => Needed in 2.X version of the provider
  tenant_id       = "<tenant id>"
  subscription_id = "<sub id>"
  client_id       = "<client id>"
  client_secret   = "<client secret>"
}