module "boundary" {
  source = "<path to module or uri>"
  project_prefix = "test-boundary"
  vmss_ssh_public_key = "<ssh public key for admin/debug>"
  admin_ips = <list of admin IPs>
  terraform_ip = <IP of terraform client>
}