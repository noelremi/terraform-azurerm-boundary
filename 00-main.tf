locals {
  resource_naming_file = var.resource_naming_file == null ? "${path.module}/azure_resource_suffix.yaml" : var.resource_naming_file
}

resource "azurerm_resource_group" "rg" {
  name     = "${var.project_prefix}-${yamldecode(file(local.resource_naming_file)).azurerm_resource_group}"
  location = var.location
}

data "azurerm_subscription" "current" {}
data "azurerm_client_config" "current" {}
