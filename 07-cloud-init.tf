# Cloud-init controller

locals {
  ## A map of variables to be used by templatefile for the config script (Bash)
  controller_config_script_variables = {

  }
  controller_config_script_content = templatefile("${path.module}/cloud-init/controller/config.sh", local.controller_config_script_variables)

  ## A map of variables to be used by templatefile for the config file (HCL)
  controller_config_file_variables = {
    keyvault_tenant_id         = data.azurerm_subscription.current.tenant_id
    keyvault_name              = azurerm_key_vault.kv.name
    keyvault_root_key_name     = azurerm_key_vault_key.root_key.name
    keyvault_worker_key_name   = azurerm_key_vault_key.worker_key.name
    keyvault_recovery_key_name = azurerm_key_vault_key.recovery_key.name
    db_boundary_user           = "${var.db_admin_username}@${split(".",azurerm_postgresql_server.db.fqdn).0}" # TODO: improve by using another user (least privilege)
    db_boundary_password       = var.db_admin_password # TODO: improve by using another user (least privilege)
    db_hostname                = azurerm_postgresql_server.db.fqdn
    db_name                    = azurerm_postgresql_database.db.name
  }
  controller_config_file_content = templatefile("${path.module}/cloud-init/controller/config.hcl", local.controller_config_file_variables)

  controller_cloud_init_variables = {
    install_script_content = base64encode(file("${path.module}/cloud-init/boundary-install"))
    config_file_content    = base64encode(local.controller_config_file_content)
  }
}

# The rendering of the controller cloud-init
data "template_cloudinit_config" "controller" {
  gzip          = false
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    content_type = "text/cloud-config"
    content      = templatefile("${path.module}/cloud-init/cloud-init.tpl", local.controller_cloud_init_variables)
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.controller_config_script_content
  }
}

# Cloud-init Worker

locals {
  ## A map of variables to be used by templatefile for the config script (Bash)
  worker_config_script_variables = {

  }
  worker_config_script_content = templatefile("${path.module}/cloud-init/worker/config.sh", local.worker_config_script_variables)

  ## A map of variables to be used by templatefile for the config file (HCL)
  worker_config_file_variables = {
    keyvault_tenant_id       = data.azurerm_subscription.current.tenant_id
    keyvault_name            = azurerm_key_vault.kv.name
    keyvault_worker_key_name = azurerm_key_vault_key.worker_key.name
  }
  worker_config_file_content = templatefile("${path.module}/cloud-init/worker/config.hcl", local.worker_config_file_variables)

  worker_cloud_init_variables = {
    install_script_content = base64encode(file("${path.module}/cloud-init/boundary-install"))
    config_file_content    = base64encode(local.worker_config_file_content)
  }
}

# The rendering of the controller cloud-init
data "template_cloudinit_config" "worker" {
  gzip          = false
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    content_type = "text/cloud-config"
    content      = templatefile("${path.module}/cloud-init/cloud-init.tpl", local.worker_cloud_init_variables)
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.worker_config_script_content
  }
}
