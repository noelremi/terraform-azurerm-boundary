# Network variables

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.project_prefix}-${yamldecode(file(local.resource_naming_file)).azurerm_virtual_network}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = var.vnet_range

  tags = var.tags
}

locals {
  service_endpoints = split("_", var.db_sku).0 == "B" ? ["Microsoft.KeyVault"] : ["Microsoft.KeyVault", "Microsoft.Sql"]
}

resource "azurerm_subnet" "controller" {
  name                 = "${var.project_prefix}-controllers-${yamldecode(file(local.resource_naming_file)).azurerm_subnet}"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.controller_subnet_range
  service_endpoints    = local.service_endpoints
}

resource "azurerm_subnet_network_security_group_association" "controller" {
  subnet_id                 = azurerm_subnet.controller.id
  network_security_group_id = azurerm_network_security_group.controller.id
}

resource "azurerm_subnet" "worker" {
  name                 = "${var.project_prefix}-workers-${yamldecode(file(local.resource_naming_file)).azurerm_subnet}"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.worker_subnet_range
  service_endpoints    = ["Microsoft.KeyVault"]
}

resource "azurerm_subnet_network_security_group_association" "worker" {
  subnet_id                 = azurerm_subnet.worker.id
  network_security_group_id = azurerm_network_security_group.worker.id
}
