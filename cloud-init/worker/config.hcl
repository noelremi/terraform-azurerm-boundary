# Worker config file

listener "tcp" {
    purpose = "proxy"
    address = "__NODE_IP__:9202"
    tls_disable = true
}

worker {
  # Name attr must be unique across workers
  name = "__NODE_NAME__"
  description = ""

  # Workers must be able to reach controllers on :9202
  controllers = [
    "10.0.0.4"
  ]

  public_addr = "__NODE_PUBLIC_IP__"
}

# must be same key as used on controller config
kms "azurekeyvault" {
  purpose        = "worker-auth"
  tenant_id      = "${keyvault_tenant_id}"
  vault_name     = "${keyvault_name}"
  key_name       = "${keyvault_worker_key_name}"
}