#!/bin/bash

/usr/local/bin/install_script "worker"

# Configure hostname in config file
sed -i "s~__NODE_NAME__~$(hostname)~" /etc/boundary/config.hcl

# Configure IP address in config file
NODE_PUBLIC_IP=$(curl --silent -H "Metadata: true" http://169.254.169.254/metadata/instance?api-version=2020-06-01 | jq -r .network.interface[].ipv4.ipAddress[].publicIpAddress)
sed -i "s~__NODE_PUBLIC_IP__~$NODE_PUBLIC_IP~" /etc/boundary/config.hcl

# Configure IP address in config file
NODE_IP=$(curl --silent -H "Metadata: true" http://169.254.169.254/metadata/instance?api-version=2020-06-01 | jq -r .network.interface[].ipv4.ipAddress[].privateIpAddress)
sed -i "s~__NODE_IP__~$NODE_IP~" /etc/boundary/config.hcl