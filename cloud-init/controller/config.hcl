# Controller config file

# Disable memory lock: https://www.man7.org/linux/man-pages/man2/mlock.2.html
disable_mlock = true

# Controller configuration block
controller {
  # This name attr must be unique across all controller instances if running in HA mode
  name = "__NODE_NAME__"
  description = ""

  # Database URL for postgres. This can be a direct "postgres://"
  # URL, or it can be "file://" to read the contents of a file to
  # supply the url, or "env://" to name an environment variable
  # that contains the URL.
  database {
      url = "postgresql://${db_boundary_user}:${db_boundary_password}@${db_hostname}:5432/${db_name}"
  }
}

# API listener configuration block
listener "tcp" {
  # Should be the address of the NIC that the controller server will be reached on
  address = "__NODE_IP_ADDR__"
  # The purpose of this listener block
  purpose = "api"

  tls_disable = true

  # Enable CORS for the Admin UI
  cors_enabled = false
//   cors_allowed_origins = ["yourcorp.yourdomain.com"]
}

# Data-plane listener configuration block (used for worker coordination)
listener "tcp" {
  # Should be the IP of the NIC that the worker will connect on
  address = "__NODE_IP_ADDR__"
  # The purpose of this listener
  purpose = "cluster"

  tls_disable = true
}

# Root KMS configuration block: this is the root key for Boundary
# Use a production KMS such as AWS KMS in production installs
kms "azurekeyvault" {
  purpose        = "root"
  tenant_id      = "${keyvault_tenant_id}"
  vault_name     = "${keyvault_name}"
  key_name       = "${keyvault_root_key_name}"
}

# Worker authorization KMS
# Use a production KMS such as AWS KMS for production installs
# This key is the same key used in the worker configuration
kms "azurekeyvault" {
  purpose        = "worker-auth"
  tenant_id      = "${keyvault_tenant_id}"
  vault_name     = "${keyvault_name}"
  key_name       = "${keyvault_worker_key_name}"
}

# Recovery KMS block: configures the recovery key for Boundary
# Use a production KMS such as AWS KMS for production installs
kms "azurekeyvault" {
  purpose        = "recovery"
  tenant_id      = "${keyvault_tenant_id}"
  vault_name     = "${keyvault_name}"
  key_name       = "${keyvault_recovery_key_name}"
}