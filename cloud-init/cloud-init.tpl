#cloud-config
package_upgrade: true
packages:
  - unzip
  - jq
write_files:
- encoding: b64
  content: ${config_file_content}
  owner: root:root
  path: /etc/boundary/config.hcl
  permissions: '0440'
- encoding: b64
  content: ${install_script_content}
  owner: root:root
  path: /usr/local/bin/install_script
  permissions: '0550'