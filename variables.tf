# Global variables

variable "project_prefix" {
  default     = "boundary"
  type        = string
  description = "The prefix used the resources naming (<prefix>-<function>-<resource type>)"
}

variable "location" {
  default = "francecentral"
  type = string
  description = "The Azure region in which the resources will be provisionned"
}

variable "resource_naming_file" {
  default = null
  type = string
  description = "A yaml file defining the resource naming in Azure. A default file is provided in the module."
}

variable "tags" {
  default = {source = "terraform"}
  type = map
  description = "A map of tags to apply to all resources"
}

variable "admin_ips" {
  type = list
  default = []
  description = "A list of ips or CIDR ranges allowed to access boundary VMs in SSH"
}

variable "terraform_ip" {
  type = string
  description = "The IP from which Terraform will call Azure IPs. Needed in the Azure Keyvault IP restrictions to allow Terraform API calls"
}

# Network variables

variable "vnet_range" {
  default = ["10.0.0.0/24"]
  type = list
  description = "The IP range for the Vnet"
}

variable "controller_subnet_range" {
  default = ["10.0.0.0/26"]
  type = list
  description = "The subnet range dedicated to the controllers"
}

variable "worker_subnet_range" {
  default = ["10.0.0.64/26"]
  type = list
  description = "The subnet range dedicated to the workers"
}

variable "db_subnet_range" {
  default = ["10.0.0.128/26"]
  type = list
  description = "The subnet range needed to provision the private endpoint of the pgsql PaaS db"
}

# VMSS config

variable "vmss_admin_username" {
  default = "boundary_admin"
  type = string
  description = "The admin username of the VMs"
}

variable "vmss_source_image_reference" {
  description = "Source image reference map"
  type        = map(string)
  default = {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

variable "vmss_ssh_public_key" {
  type = string
  description = "The SSH public key to connect to the VMSS"
}

variable "vmms_managed_disk_type" {
  default = "Premium_LRS"
  type = string
  description = "The managed disk type to use for the VMSS"
}

## controller variables

variable "controller_instance_size" {
  default = "Standard_DS1_v2"
  type = string
  description = "The Azure VM instance size for the controller cluster"
}

variable "controller_instance_count" {
  default = 1
  type = number
  description = "The number of controllers in the cluster"
}

## worker variables

variable "worker_instance_size" {
  default = "Standard_DS1_v2"
  type = string
  description = "The Azure VM instance size for the worker clsuer"
}

variable "worker_instance_count" {
  default = 1
  type = number
  description = "The number of workers in the cluster"
}

# DB variables

variable "db_admin_username" {
  default = "db_admin"
  type = string
  description = "The admin username of the pgsql db"
}

variable "db_admin_password" {
  default = "ToB3Chang3d2020ffdqsfdsqf"
  type = string
  description = "The admin password of the pgsql db"
}

variable "db_sku" {
  default = "B_Gen5_2"
  type = string
  description = "The SKU of the Azure Database for PostgreSQL {pricing tier}{compute generation}{vCores}(https://docs.microsoft.com/en-us/azure/postgresql/concepts-pricing-tiers)"
}

# KMS (Keyvault)

# variable "boundary_key_name" {
#   default = "boundarykey"
#   type = string
#   description = "The name of the key needed by boundary"
# }