resource "azurerm_key_vault" "kv" {
  name                        = "${var.project_prefix}-${yamldecode(file(local.resource_naming_file)).azurerm_key_vault}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  enabled_for_disk_encryption = false
  tenant_id                   = data.azurerm_subscription.current.tenant_id
  soft_delete_enabled         = false # TODO : turn to true once Terraform is working
  purge_protection_enabled    = false # TODO : turn to true once Terraform is working
  sku_name                    = "standard"
  enabled_for_deployment      = false
  tags                        = var.tags

  network_acls {
    bypass                     = "None"
    default_action             = "Deny"
    virtual_network_subnet_ids = [azurerm_subnet.controller.id, azurerm_subnet.worker.id]
    ip_rules                   = [var.terraform_ip] # We need to add the ip from which Terraform makes its API calls
  }
}


## Policies

resource "azurerm_key_vault_access_policy" "controller" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_subscription.current.tenant_id
  object_id    = azurerm_linux_virtual_machine_scale_set.controller_cluster.identity[0].principal_id

  key_permissions = [ # TODO : restrict if possible
    "get",
    "list",
    "sign",
    "unwrapKey",
    "wrapKey",
    "decrypt"
  ]
}

resource "azurerm_key_vault_access_policy" "worker" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_subscription.current.tenant_id
  object_id    = azurerm_linux_virtual_machine_scale_set.worker_cluster.identity[0].principal_id

  key_permissions = [ # TODO : restrict if possible
    "get",
    "list",
    "sign",
    "unwrapKey",
    "wrapKey",
    "decrypt"
  ]
}

resource "azurerm_key_vault_access_policy" "terraform" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_subscription.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  key_permissions = [
    "get",
    "list",
    "create",
    "encrypt",
    "sign",
    "unwrapKey",
    "wrapKey",
    "decrypt",
    "update",
  ]
}

# Root KMS configuration block: this is the root key for Boundary
resource "azurerm_key_vault_key" "root_key" {
  name         = "root"
  key_vault_id = azurerm_key_vault.kv.id
  key_type     = "RSA"
  key_size     = 4096

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

# Worker authorization KMS
# This key is the same key used in the worker configuration
resource "azurerm_key_vault_key" "worker_key" {
  name         = "worker"
  key_vault_id = azurerm_key_vault.kv.id
  key_type     = "RSA"
  key_size     = 4096

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

# Recovery KMS block: configures the recovery key for Boundary
resource "azurerm_key_vault_key" "recovery_key" {
  name         = "recovery"
  key_vault_id = azurerm_key_vault.kv.id
  key_type     = "RSA"
  key_size     = 4096

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}
