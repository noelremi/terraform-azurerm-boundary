# Controller NSG

resource "azurerm_network_security_group" "controller" {
  name                = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_network_security_group}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  tags = var.tags
}

# Allow traffic from the Internet to the controller (through the ELB)
resource "azurerm_network_security_rule" "internet_to_contoller_allow_9200_IN" {
  name                        = "OK-Internet-Contoller-9200"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_address_prefix       = "*"
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_range      = "9200"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.controller.name
}

# Allow traffic from the workers to the controllers
resource "azurerm_network_security_rule" "worker_to_contoller_allow_9201_IN" {
  name                        = "OK-Worker-Contoller-9201"
  priority                    = 110
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_address_prefixes     = azurerm_subnet.worker.address_prefixes
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_range      = "9201"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.controller.name
}

# Allow traffic from admin IPs to the controller
resource "azurerm_network_security_rule" "admin_ips_to_contoller_allow_22_IN" {
  name                        = "OK-Admin-Contoller-22"
  priority                    = 120
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_address_prefixes       = var.admin_ips
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_range      = "22"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.controller.name
}

# Worker NSG

resource "azurerm_network_security_group" "worker" {
  name                = "${var.project_prefix}-worker-${yamldecode(file(local.resource_naming_file)).azurerm_network_security_group}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  tags = var.tags
}

# Allow traffic from the Internet to the worker (through the ELB)
resource "azurerm_network_security_rule" "internet_to_contoller_allow_9202_IN" {
  name                        = "OK-Internet-Contoller-9202"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_address_prefix       = "*"
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_range      = "9202"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.worker.name
}

# Allow traffic from admin IPs to the worker
resource "azurerm_network_security_rule" "admin_ips_to_worker_allow_22_IN" {
  name                        = "OK-Admin-Worker-22"
  priority                    = 120
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_address_prefixes       = var.admin_ips
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_range      = "22"
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.worker.name
}