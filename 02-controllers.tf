# External load balancer configuration (to expose API to clients)

resource "azurerm_public_ip" "controller" {
  name                = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_public_ip}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.tags
}

resource "azurerm_lb" "controller" {
  name                = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_lb_external}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_frontend_ip_configuration}"
    public_ip_address_id = azurerm_public_ip.controller.id
  }

  tags = var.tags
}

resource "azurerm_lb_backend_address_pool" "controller" {
  resource_group_name = azurerm_resource_group.rg.name
  loadbalancer_id     = azurerm_lb.controller.id
  name                = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_backend_address_pool}"
}

resource "azurerm_lb_rule" "controller" {
  resource_group_name            = azurerm_resource_group.rg.name
  loadbalancer_id                = azurerm_lb.controller.id
  name                           = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_rule}"
  protocol                       = "TCP"
  frontend_port                  = 443
  backend_port                   = 9200
  probe_id                       = azurerm_lb_probe.controller.id
  backend_address_pool_id        = azurerm_lb_backend_address_pool.controller.id
  frontend_ip_configuration_name = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_frontend_ip_configuration}"
}

resource "azurerm_lb_probe" "controller" {
  resource_group_name = azurerm_resource_group.rg.name
  loadbalancer_id     = azurerm_lb.controller.id
  name                = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_probe}"
  port                = 9200
  request_path        = "/"
  protocol            = "HTTP"
}

resource "azurerm_lb_nat_pool" "controller" {
  resource_group_name            = azurerm_resource_group.rg.name
  loadbalancer_id                = azurerm_lb.controller.id
  name                           = "Admin"
  protocol                       = "Tcp"
  frontend_port_start            = 2200
  frontend_port_end              = 2205
  backend_port                   = 22
  frontend_ip_configuration_name = "${var.project_prefix}-controller-elb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_frontend_ip_configuration}"
}

# Internal load balancer configuration for traffic between the workers and the controllers

resource "azurerm_lb" "controller_ilb" {
  name                = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_lb_internal}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                          = "${var.project_prefix}-controller-ilb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_frontend_ip_configuration}"
    subnet_id                     = azurerm_subnet.controller.id
    private_ip_address_allocation = "Static"
  }

  tags = var.tags
}

resource "azurerm_lb_backend_address_pool" "controller_ilb" {
  resource_group_name = azurerm_resource_group.rg.name
  loadbalancer_id     = azurerm_lb.controller_ilb.id
  name                = "${var.project_prefix}-controller-ilb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_backend_address_pool}"
}

resource "azurerm_lb_rule" "controller_ilb" {
  resource_group_name            = azurerm_resource_group.rg.name
  loadbalancer_id                = azurerm_lb.controller_ilb.id
  name                           = "${var.project_prefix}-controller-ilb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_rule}"
  protocol                       = "TCP"
  frontend_port                  = 9201
  backend_port                   = 9201
  probe_id                       = azurerm_lb_probe.controller_ilb.id
  backend_address_pool_id        = azurerm_lb_backend_address_pool.controller_ilb.id
  frontend_ip_configuration_name = "${var.project_prefix}-controller-ilb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_frontend_ip_configuration}"
}

resource "azurerm_lb_probe" "controller_ilb" {
  resource_group_name = azurerm_resource_group.rg.name
  loadbalancer_id     = azurerm_lb.controller.id
  name                = "${var.project_prefix}-controller-ilb-${yamldecode(file(local.resource_naming_file)).azurerm_lb_probe}"
  port                = 9201
  protocol            = "TCP"
}

# Virtual machine scale set configuration
resource "azurerm_linux_virtual_machine_scale_set" "controller_cluster" {
  name                 = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_linux_virtual_machine_scale_set}"
  resource_group_name  = azurerm_resource_group.rg.name
  location             = azurerm_resource_group.rg.location
  sku                  = var.controller_instance_size
  instances            = var.controller_instance_count
  admin_username       = var.vmss_admin_username
  provision_vm_agent   = true
  computer_name_prefix = "boundary-controller-node"
  overprovision        = false

  tags = var.tags

  source_image_reference {
    publisher = var.vmss_source_image_reference.publisher
    offer     = var.vmss_source_image_reference.offer
    sku       = var.vmss_source_image_reference.sku
    version   = var.vmss_source_image_reference.version
  }

  # Authentication configuration
  disable_password_authentication = true
  admin_ssh_key {
    public_key = var.vmss_ssh_public_key
    username   = var.vmss_admin_username
  }

  # Rolling upgrade policy configuration
  upgrade_mode    = "Rolling"
  scale_in_policy = "OldestVM"

  rolling_upgrade_policy {
    max_batch_instance_percent              = 100
    max_unhealthy_instance_percent          = 100
    max_unhealthy_upgraded_instance_percent = 100
    pause_time_between_batches              = "PT0S"
  }

  health_probe_id = azurerm_lb_probe.controller.id # required when using rolling upgrade policy

  #   secret { # Wildcard cert
  #     key_vault_id = azurerm_key_vault.kv_cert.id
  #     certificate {
  #       url = "${azurerm_key_vault.kv_cert.vault_uri}secrets/${var.front_wildcard_certificate_name}/${var.front_wildcard_certificate_version}"
  #     }
  #   }

  identity {
    type = "SystemAssigned"
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.vmss_boot_diag.primary_blob_endpoint
  }

  # OS configuration
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = var.vmms_managed_disk_type
  }

  custom_data = data.template_cloudinit_config.controller.rendered

  # Network configuration
  network_interface {
    name    = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_linux_virtual_machine_scale_set_network_interface}"
    primary = true

    ip_configuration {
      name                                   = "${var.project_prefix}-controller-${yamldecode(file(local.resource_naming_file)).azurerm_linux_virtual_machine_scale_set_ip_configuration}"
      subnet_id                              = azurerm_subnet.controller.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.controller.id,azurerm_lb_backend_address_pool.controller_ilb.id]
      load_balancer_inbound_nat_rules_ids    = [azurerm_lb_nat_pool.controller.id]
      primary                                = true
    }
  }

  # Zone configuration
  zones        = ["1", "2", "3"]
  zone_balance = true
}

# ## Diag settings
# module "diag_vmss" {
#   source                     = "git@code.livexp.fr:modules/terraform-azurerm-diagnostic-setting.git"
#   basic_inputs               = module.base.basic_inputs
#   resource_id                = azurerm_linux_virtual_machine_scale_set.vault_cluster.id
#   log_analytics_workspace_id = module.base.log_analytics.resource.id
#   log_categories             = ["FrontdoorAccessLog", "FrontdoorWebApplicationFirewallLog"]
# }

locals {
  # We remove "-" and "_" from the prefix 
  storage_account_prefix = replace(replace(var.project_prefix, "-", ""), "_", "")
}


resource "azurerm_storage_account" "vmss_boot_diag" {
  name                     = substr("${local.storage_account_prefix}boot${yamldecode(file(local.resource_naming_file)).azurerm_storage_account}", 0, 24)
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = var.tags
}
