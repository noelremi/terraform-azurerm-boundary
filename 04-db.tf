resource "azurerm_postgresql_server" "db" {
  name                = "${var.project_prefix}-db-${yamldecode(file(local.resource_naming_file)).azurerm_postgresql_server}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  administrator_login          = var.db_admin_username
  administrator_login_password = var.db_admin_password

  sku_name   = var.db_sku
  version    = "11"
  storage_mb = 5120

  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true

#   public_network_access_enabled    = false
  ssl_enforcement_enabled          = true
  ssl_minimal_tls_version_enforced = "TLS1_2"
  tags                             = var.tags
}

resource "azurerm_postgresql_database" "db" {
  name                = "boundary"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_postgresql_server.db.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "azurerm_postgresql_firewall_rule" "db" {
  name                = "controller"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_postgresql_server.db.name
  start_ip_address    = azurerm_public_ip.controller.ip_address 
  end_ip_address      = azurerm_public_ip.controller.ip_address
}