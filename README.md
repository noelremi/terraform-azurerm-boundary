# terraform-azurerm-boundary

This version is not usable just yet. TODO:
* Implement SSL
* Reference controllers IP in workers config automatically (or use the LB maybe?)
* Cleanup the code
* Add more variables to customize the infrastructure
* Documentation

```
module "boundary" {
  source = "<path to module or uri>"
  project_prefix = "test-boundary"
  vmss_ssh_public_key = "<ssh public key for admin/debug>"
  admin_ips = <list of admin IPs>
  terraform_ip = <IP of terraform client>
}
```